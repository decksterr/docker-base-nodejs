FROM debian:stretch-slim

# Base image for node lts/current

SHELL ["/bin/bash", "-c"]

RUN deps="curl software-properties-common" \
    && apt-get update && apt-get upgrade -y \
    && apt-get install -y $deps \
    # lts
    && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    # current
    # && curl -sL https://deb.nodesource.com/setup_12.x | bash - \
    && apt-get install -y nodejs \
    && apt-get purge -y --auto-remove $deps

# lts-dev / current-dev
# RUN npm i -g node-dev
